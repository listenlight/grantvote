# frozen_string_literal: false

require "irb/completion"
# require "rubygems"
require "hirb"
require "wirble"
require "awesome_print"

AwesomePrint.irb!

# if ENV["RAILS_ENV"]
#   IRB.conf[:IRB_RC] = proc do
#     class IRB
#       class WorkSpace
#         alias orig_filter_backtrace filter_backtrace

#         def filter_backtrace(cln)
#           filtered_bt = orig_filter_backtrace(cln)
#           rails_backtrace_cleaner.clean(Array(filtered_bt)).first
#         end

#         private
#           def rails_backtrace_cleaner
#             bc = ActiveSupport::BacktraceCleaner.new
#             bc.add_filter   { |line| line.gsub(Rails.root.to_s, "<root>") }
#             bc.add_silencer { |line| line.index("<root>").nil? && line.index("/").zero? }
#             bc.add_silencer { |line| line.index("<root>/vendor/").zero? }
#             # bc.add_silencer { |line| line.include? "console.rb" }
#             bc.add_silencer { |line| line.include? "ruby-debug.ide.rb" }
#             bc.add_silencer { |line| line.include? "rdebug-ide" }
#             bc.add_silencer { |line| line.include? "appsignal" }
#             bc.add_silencer { |line| line.include? "cache" }
#             bc.add_silencer { |line| line.include? "spring" }
#             bc.add_silencer { |line| line.include? "rbenv" }
#           end
#       end
#     end
#   end
# end

ActiveRecord::Base.logger.level = 1 if defined?(ActiveRecord)
IRB.conf[:SAVE_HISTORY] = 1000

# Overriding Object class
class Object
  # Easily print methods local to an object's class
  def lm
    (methods - Object.instance_methods).sort
  end

  # look up source location of a method
  def sl(method_name)
    method(method_name).source_location
  rescue StandardError
    "#{method_name} not found"
  end

  # open particular method in vs code
  def ocode(method_name)
    file, line = sl(method_name)
    if file && line
      `code -g '#{file}:#{line}'`
    else
      "'#{method_name}' not found :(Try #{name}.lm to see available methods"
    end
  end

  # display method source in rails console
  def ds(method_name)
    method(method_name).source.display
  end

  # open json object in VS Code Editor
  def oo
    tempfile = Rails.root.join("tmp/#{SecureRandom.hex}")
    File.open(tempfile, "w") { |f| f << as_json }
    system("#{'code' || 'nano'} #{tempfile}")
    sleep(1)
    File.delete(tempfile)
  end
end

# history command
def hist(count = 0)
  # Get history into an array
  history_array = Readline::HISTORY.to_a

  # if count is > 0 we'll use it.
  # otherwise set it to 0
  count = 0 unless count.positive?

  if count.positive?
    from = history_array.length - count
    history_array = history_array[from..]
  end

  print(history_array.join("\n"))
end

# copy a string to the clipboard
def cp(string)
  `echo "#{string}" | pbcopy`
  puts("copied in clipboard")
end

# reloads the irb console can be useful for debugging .irbrc
def reload_irb
  load(File.expand_path("~/.irbrc"))

  # will reload rails env if you are running ./script/console
  reload! if @script_console_running
  puts("Console Reloaded!")
end

# opens irbrc in vscode
def edit_irb
  `code ~/.irbrc` if system("code")
end

# From http://blog.evanweaver.com/articles/2006/12/13/benchmark/
# Call benchmark { } with any block and you get the wallclock runtime
# as well as a percent change + or - from the last run
# rubocop:disable Style/GlobalVars
def bm
  cur = Time.zone.now
  result = yield
  print("#{cur = Time.zone.now - cur} seconds")
  begin
    puts(" (#{Integer((cur / $last_benchmark * 100), 10) - 100}% change)")
  rescue StandardError
    puts("")
  end
  $last_benchmark = cur
  result
end
# rubocop:enable Style/GlobalVars

# exit using `q`
alias q exit

# all available methods explaination
def ll
  puts(
    "============================================================================================================="
  )
  puts(
    "Welcome to rails console. Here are few list of pre-defined methods you can use."
  )
  puts(
    "============================================================================================================="
  )
  puts("obj.sl(:method) ------> source location e.g lead.sl(:name)")
  puts("obj.ocode(:method) ---> open method in vs code e.g lead.ocode(:name)")
  puts(
    "obj.dispsoc(:method) -> display method source in rails console e.g lead.dispsoc(:name)"
  )
  puts("obj.oo ---------------> open object json in vs code e.g lead.oo")
  puts("hist(n) --------------> command history e.g hist(10)")
  puts("cp(str) --------------> copy string in clipboard e.g cp(lead.name)")
  puts(
    "bm(block) ------------> benchmarking for block passed as an argument e.g bm { Lead.all.pluck(:stage);0 }"
  )
  puts(
    "============================================================================================================="
  )
end
