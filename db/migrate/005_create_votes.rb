class CreateVotes < ActiveRecord::Migration
  def self.up
    create_table :votes do |t|
      t.integer :user_id, :null => false
      t.integer :blitz_id, :default => 0, :null => false
      t.integer :group_id, :default => 0, :null => false
      t.integer :grant_id, :default => 0, :null => false
      t.string :cast, :null => false
      t.timestamps
    end
    add_index :votes, :user_id
    add_index :votes, :blitz_id
    add_index :votes, [:group_id, :grant_id]
  end

  def self.down

    drop_table :votes
  end
end
