class PasswordResetsController < ApplicationController
  before_filter :require_no_user
  before_filter :load_user_using_perishable_token, :only => [:edit, :update]
  before_filter :verify_authenticity_token

  def new
    @page_title = "Reset Password"
    render
  end

  def create
    @user = User.find_by_email(params[:email])
    if @user
      @user.deliver_password_reset_instructions!
      flash[:notice] = "Please check your email for instructions " +
                          "to reset your password. "
      redirect_to root_url
    else
      flash[:notice] = "Account for email address was not found. "
      render :action => :new
    end
  end

  def edit
    @page_title = "Reset Password"
    render
  end

  def update
    @user.password = params[:user][:password]
    @user.password_confirmation = params[:user][:password_confirmation]
    if @user.save
      flash[:notice] = "Updated password. "
      redirect_to account_url
    else
      render :action => :edit
    end
  end

  private

    def load_user_using_perishable_token
      @user = User.find_using_perishable_token(params[:id])
      unless @user
        flash[:notice] = "We're sorry, but we could not locate your account. " +
          "If you are having issues try copying and pasting the URL " +
          "from your email into your browser or restarting the " +
          "reset password process."
        redirect_to root_url
      end
    end

end

